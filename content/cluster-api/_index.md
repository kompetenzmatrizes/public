---
title: "Cluster Applikationsentwicklung"
date: 2017-10-17T15:26:15Z
draft: false
weight: 10
---

## 1 Lehrjahr

[M319 Applikationen entwerfen und implementieren]({{< ref "/319.md" >}})  
[M293 Einstieg Webentwicklung]({{< ref "/293.md" >}})

## 2 Lehrjahr

[M320 Objektorientiert Programmieren]({{< ref "/319.md" >}})  
[M322 Benutzerschnittstellen entwerfen und implementieren]({{< ref "/322.md" >}})  
[M426 Software mit agilen Methoden entwickeln]({{< ref "/426.md" >}})

## 3 Lehrjahr

[M183 Applikationssicherheit implementieren]({{< ref "/183.md" >}})  
[M323 Funktional Programmieren]({{< ref "/323.md" >}})  
[M450 Applikationen testen]({{< ref "/450.md" >}})

## 4 Lehrjahr

[M321 Verteilte Systeme programmieren]({{< ref "/321.md" >}})
