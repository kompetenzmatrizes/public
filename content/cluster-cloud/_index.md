---
title: "Cluster Cloud Computing"
draft: false
weight: 13
---

## 1 Lehrjahr

-

## 2 Lehrjahr

[M169 Dienste mit Containern bereitstellen]({{< ref "/169.md" >}})  
[M346 Cloudlösungen konzipieren und realisieren]({{< ref "/346.md" >}})  
[M347 Dienst mit Container anwenden]({{< ref "/347.md" >}})  

## 3 Lehrjahr

-

## 4 Lehrjahr

[M324 DevOps-Prozesse mit Tools unterstützen]({{< ref "/324.md" >}})
