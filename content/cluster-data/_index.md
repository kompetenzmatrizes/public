---
title: "Cluster Daten und Datenhaltung"
date: 2021-04-12T08:04:26Z
draft: false
weight: 12
---

## 1 Lehrjahr

[M162 Daten analysieren und modellieren]({{< ref "/162.md" >}})  
[M164 Datenbanken erstellen und Daten einfügen]({{< ref "/164.md" >}})  
[M231 Datenschutz und Datensicherheit anwenden"]({{< ref "/231.md" >}})  


## 2 Lehrjahr

[M114 Codierungs- Kompressions- und Verschlüsselungsverfahren einsetzen]({{< ref "/114.md" >}})  
[M165 NoSQL-Datenbanken einsetzen]({{< ref "/165.md" >}})  

## 3 Lehrjahr

-

## 4 Lehrjahr

-
