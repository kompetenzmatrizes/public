+++
title = "Nach Ausbildungsjahr Applikationsentwickler"
date = "2021-04-12"
menu = "main"
+++

## 1 Lehrjahr

[M319 Applikationen entwerfen und implementieren]({{< ref "/319.md" >}})  
[M293 Einstieg Webentwicklung]({{< ref "/293.md" >}})  
[M162 Daten analysieren und modellieren]({{< ref "/162.md" >}})  
[M164 Datenbanken erstellen und Daten einfügen]({{< ref "/164.md" >}})  
[M231 Datenschutz und Datensicherheit anwenden"]({{< ref "/231.md" >}})  
[M431 Aufträge im eigenen Berufsumfeld selbständig durchführen]({{< ref "/431.md" >}})  
[M117 Informatik- und Netzinfrastruktur für ein kleines Unternehmen realisieren]({{< ref "/117.md" >}})  
[M122 Abläufe mit einer Scriptsprache automatisieren]({{< ref "/122.md" >}})  

## 2 Lehrjahr

[M320 Objektorientiert Programmieren]({{< ref "/320.md" >}})  
[M322 Benutzerschnittstellen entwerfen und implementieren]({{< ref "/322.md" >}})  
[M426 Software mit agilen Methoden entwickeln]({{< ref "/426.md" >}})  
[M346 Cloudlösungen konzipieren und realisieren]({{< ref "/346.md" >}})  
[M347 Dienst mit Container anwenden]({{< ref "/347.md" >}})  
[M114 Codierungs- Kompressions- und Verschlüsselungsverfahren einsetzen]({{< ref "/114.md" >}})  
[M165 NoSQL-Datenbanken einsetzen]({{< ref "/165.md" >}})  
[M254 Geschäftsprozesse im eigenen Berufsumfeld beschreiben]({{< ref "/254.md" >}})  

## 3 Lehrjahr

[M183 Applikationssicherheit implementieren]({{< ref "/183.md" >}})  
[M323 Funktional Programmieren]({{< ref "/323.md" >}})  
[M450 Applikationen testen]({{< ref "/450.md" >}})  
[M306 Kleinprojekte im eigenen Berufsumfeld abwickeln]({{< ref "/306.md" >}})  

## 4 Lehrjahr

[M321 Verteilte Systeme programmieren]({{< ref "/321.md" >}})  
[M324 DevOps-Prozesse mit Tools unterstützen]({{< ref "/324.md" >}})  
[M245 Innovative ICT-Lösungen umsetzen]({{< ref "/254.md" >}})  
[M241 Innovative ICT-Lösungen initialisieren]({{< ref "/241.md" >}})  
