---
title: "Cluster Plattformentwicklung"
date: 2017-10-17T15:26:15Z
draft: false
weight: 11
---

## 1 Lehrjahr

[M117 Informatik- und Netzinfrastruktur für ein kleines Unternehmen realisieren]({{< ref "/117.md" >}})  
[M122 Abläufe mit einer Scriptsprache automatisieren]({{< ref "/122.md" >}})  
[M123 Serverdienste in Betrieb nehmen]({{< ref "/123.md" >}})  

## 2 Lehrjahr

[M129 LAN-Komponenten in Betrieb nehmen]({{< ref "/129.md" >}})  
[M141 Datenbanksystem in Betrieb nehmen]({{< ref "/141.md" >}})  
[M143 Backup- und Restore-Systeme implementieren]({{< ref "/143.md" >}})  

## 3 Lehrjahr

[M145 Netzwerk betreiben und erweitern]({{< ref "/145.md" >}})  
[M159 Directory Services konfigurieren und in Betrieb nehmen]({{< ref "/159.md" >}})  
[M300 Plattformübergreifende Dienste in ein Netzwerk integrieren]({{< ref "/300.md" >}})  

## 4 Lehrjahr

[M182 Systemsicherheit implementieren]({{< ref "/182.md" >}})
