---
title: "Cluster Organisation und Innovation"
date: 2017-10-17T15:26:15Z
draft: false
weight: 14
---

## 1 Lehrjahr

[M431 Aufträge im eigenen Berufsumfeld selbständig durchführen]({{< ref "/431.md" >}})  

## 2 Lehrjahr

[M254 Geschäftsprozesse im eigenen Berufsumfeld beschreiben]({{< ref "/254.md" >}})  
[M158 Software-Migration planen und durchführen]({{< ref "/158.md" >}})  
[M157 IT-System-Einführung planen und durchführen]({{< ref "/157.md" >}})  

## 3 Lehrjahr

[M306 Kleinprojekte im eigenen Berufsumfeld abwickeln]({{< ref "/306.md" >}})  

## 4 Lehrjahr

[M245 Innovative ICT-Lösungen umsetzen]({{< ref "/254.md" >}})  
[M241 Innovative ICT-Lösungen initialisieren]({{< ref "/241.md" >}})  
