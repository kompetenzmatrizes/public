+++
title = "Nach Ausbildungsjahr Platformentwickler"
date = "2021-04-12"
menu = "main"
+++

## 1 Lehrjahr

[M319 Applikationen entwerfen und implementieren]({{< ref "/319.md" >}})  
[M162 Daten analysieren und modellieren]({{< ref "/162.md" >}})  
[M164 Datenbanken erstellen und Daten einfügen]({{< ref "/164.md" >}})  
[M231 Datenschutz und Datensicherheit anwenden"]({{< ref "/231.md" >}})  
[M431 Aufträge im eigenen Berufsumfeld selbständig durchführen]({{< ref "/431.md" >}})  
[M117 Informatik- und Netzinfrastruktur für ein kleines Unternehmen realisieren]({{< ref "/117.md" >}})  
[M122 Abläufe mit einer Scriptsprache automatisieren]({{< ref "/122.md" >}})  
[M123 Serverdienste in Betrieb nehmen]({{< ref "/123.md" >}})  

## 2 Lehrjahr

[M169 Dienste mit Containern bereitstellen]({{< ref "/169.md" >}})  
[M346 Cloudlösungen konzipieren und realisieren]({{< ref "/346.md" >}})  
[M114 Codierungs- Kompressions- und Verschlüsselungsverfahren einsetzen]({{< ref "/114.md" >}})  
[M254 Geschäftsprozesse im eigenen Berufsumfeld beschreiben]({{< ref "/254.md" >}})  
[M158 Software-Migration planen und durchführen]({{< ref "/158.md" >}})  
[M129 LAN-Komponenten in Betrieb nehmen]({{< ref "/129.md" >}})  
[M141 Datenbanksystem in Betrieb nehmen]({{< ref "/141.md" >}})  
[M143 Backup- und Restore-Systeme implementieren]({{< ref "/143.md" >}})

## 3 Lehrjahr

[M306 Kleinprojekte im eigenen Berufsumfeld abwickeln]({{< ref "/306.md" >}})  
[M145 Netzwerk betreiben und erweitern]({{< ref "/145.md" >}})  
[M159 Directory Services konfigurieren und in Betrieb nehmen]({{< ref "/159.md" >}})  
[M300 Plattformübergreifende Dienste in ein Netzwerk integrieren]({{< ref "/300.md" >}})  

## 4 Lehrjahr

[M245 Innovative ICT-Lösungen umsetzen]({{< ref "/254.md" >}})  
[M241 Innovative ICT-Lösungen initialisieren]({{< ref "/241.md" >}})  
[M182 Systemsicherheit implementieren]({{< ref "/182.md" >}})  
[M157 IT-System-Einführung planen und durchführen]({{< ref "/157.md" >}}) 
