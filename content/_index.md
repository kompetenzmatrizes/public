---
title: Cluster
---

# Kompetenzmatrizes der ICT-Module

Öffentlich zugängliche Kompetenzmatrizes des Projekt Modulentwicklung für die ICT-Module der Fachrichtungen Applikationsentwicklung und Plattformentwicklung nach BiVo2021

## Cluster Applikationsentwicklung

[M319 Applikationen entwerfen und implementieren]({{< ref "/319.md" >}})  
[M293 Einstieg Webentwicklung]({{< ref "/293.md" >}})  
[M320 Objektorientiert Programmieren]({{< ref "/320.md" >}})  
[M322 Benutzerschnittstellen entwerfen und implementieren]({{< ref "/322.md" >}})  
[M426 Software mit agilen Methoden entwickeln]({{< ref "/426.md" >}})  
[M183 Applikationssicherheit implementieren]({{< ref "/183.md" >}})  
[M323 Funktional Programmieren]({{< ref "/323.md" >}})  
[M450 Applikationen testen]({{< ref "/450.md" >}})  
[M321 Verteilte Systeme programmieren]({{< ref "/321.md" >}})

## Cluster Plattformentwicklung

[M117 Informatik- und Netzinfrastruktur für ein kleines Unternehmen realisieren]({{< ref "/117.md" >}})  
[M122 Abläufe mit einer Scriptsprache automatisieren]({{< ref "/122.md" >}})  
[M123 Serverdienste in Betrieb nehmen]({{< ref "/123.md" >}})  
[M129 LAN-Komponenten in Betrieb nehmen]({{< ref "/129.md" >}})  
[M141 Datenbanksystem in Betrieb nehmen]({{< ref "/141.md" >}})  
[M143 Backup- und Restore-Systeme implementieren]({{< ref "/143.md" >}})  
[M145 Netzwerk betreiben und erweitern]({{< ref "/145.md" >}})  
[M159 Directory Services konfigurieren und in Betrieb nehmen]({{< ref "/159.md" >}})  
[M300 Plattformübergreifende Dienste in ein Netzwerk integrieren]({{< ref "/300.md" >}})  
[M182 Systemsicherheit implementieren]({{< ref "/182.md" >}})

## Cluster Daten und Datenhaltung

[M162 Daten analysieren und modellieren]({{< ref "/162.md" >}})  
[M164 Datenbanken erstellen und Daten einfügen]({{< ref "/164.md" >}})  
[M231 Datenschutz und Datensicherheit anwenden"]({{< ref "/231.md" >}})  
[M114 Codierungs- Kompressions- und Verschlüsselungsverfahren einsetzen]({{< ref "/114.md" >}})  
[M165 NoSQL-Datenbanken einsetzen]({{< ref "/165.md" >}})  

## Cluster Cloud Computing

[M169 Dienste mit Containern bereitstellen]({{< ref "/169.md" >}})  
[M346 Cloudlösungen konzipieren und realisieren]({{< ref "/346.md" >}})  
[M347 Dienst mit Container anwenden]({{< ref "/347.md" >}})  
[M324 DevOps-Prozesse mit Tools unterstützen]({{< ref "/324.md" >}})

## Cluster Organisation und Innovation

[M431 Aufträge im eigenen Berufsumfeld selbständig durchführen]({{< ref "/431.md" >}})  
[M254 Geschäftsprozesse im eigenen Berufsumfeld beschreiben]({{< ref "/254.md" >}})  
[M158 Software-Migration planen und durchführen]({{< ref "/158.md" >}})  
[M157 IT-System-Einführung planen und durchführen]({{< ref "/157.md" >}})  
[M306 Kleinprojekte im eigenen Berufsumfeld abwickeln]({{< ref "/306.md" >}})  
[M245 Innovative ICT-Lösungen umsetzen]({{< ref "/254.md" >}})  
[M241 Innovative ICT-Lösungen initialisieren]({{< ref "/241.md" >}})

## Cluster Mathematik

[Mathe 1]({{< ref "/M1.md" >}})  
[Mathe 2]({{< ref "/M2.md" >}})  
[Mathe 3]({{< ref "/M2.md" >}})
